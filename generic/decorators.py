# -*- coding: utf-8 -*-
"""
.. module:: generic.decorators
   :synopsis: Decorators for use throughout the project.

.. moduleauthor:: Mark McKinlay <mark.mckinlay.io>
"""

from django.core.exceptions import PermissionDenied


def superuser_only(application):
    """
    Limit view to superusers only.

    Usage:
    --------------------------------------------------------------------------
    @superuser_only
    def my_view(request):
        ...
    --------------------------------------------------------------------------

    or in urls:

    --------------------------------------------------------------------------
    urlpatterns = patterns('',
        (r'^foobar/(.*)', is_staff(my_view)),
    )
    --------------------------------------------------------------------------    
    """

    def _inner(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return application(request, *args, **kwargs)

    return _inner
