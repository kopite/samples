# -*- coding: utf-8 -*-
"""
.. module:: clients.models
   :synopsis: Models for the Clients App

.. moduleauthor:: Mark McKinlay <mark.mckinlay.io>
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Client(models.Model):
    full_name = models.CharField(_('full name'),
                                 max_length=50,
                                 blank=False
                                 )
