# -*- coding: utf-8 -*-
"""
.. module:: clients.admin
   :synopsis: Admin config for the Clients app.

.. moduleauthor:: Mark McKinlay <mark.mckinlay.io>
"""
from django.contrib import admin
from .models import Client


class ClientAdmin(admin.ModelAdmin):
    list_display = ['id',
                    'full_name',
                    ]


admin.site.register(Client, ClientAdmin)
