# -*- coding: utf-8 -*-
"""
.. module:: samples.models
   :synopsis: Models for the Samples App

.. moduleauthor:: Mark McKinlay <mark.mckinlay.io>
"""

from django.db import models


class ClientSample(models.Model):
    client_id = models.ForeignKey('clients.Client',
                                  on_delete=models.CASCADE)
    result = models.DecimalField(
        max_digits=10,
        decimal_places=4,
        default=0.0)
    unit = models.CharField(max_length=10)
