# -*- coding: utf-8 -*-
"""
.. module:: samples.admin
   :synopsis: Admin config for the samples app.

.. moduleauthor:: Mark McKinlay <mark.mckinlay.io>
"""

from django.contrib import admin
from .models import ClientSample


class ClientSampleAdmin(admin.ModelAdmin):
    list_display = ['pk',
                    'client_id',
                    'result',
                    'unit',
                    ]
    pass


admin.site.register(ClientSample, ClientSampleAdmin)
