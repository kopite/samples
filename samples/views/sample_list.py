from django.views.generic import ListView
from samples.models import ClientSample


class ShowSamples(ListView):
    model = ClientSample
    template_name = 'samples.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        queryset = ClientSample.objects.all().order_by('client_id', '-pk')\
            .distinct('client_id')
        return queryset
