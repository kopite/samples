from django.views.generic.edit import FormView
from samples.forms import ImportSamplesForm
from samples.models import ClientSample
from clients.models import Client
from django.urls import reverse_lazy


class ImportSamples(FormView):
    template_name = 'import_samples.html'
    form_class = ImportSamplesForm
    success_url = reverse_lazy('samples:show-samples')

    def form_valid(self, form):
        data = form.cleaned_data['sample_data']
        for sample in data:
            client = Client.objects.get(id=sample[0])
            sample_model = ClientSample.objects.create(client_id=client,
                                                       result=sample[1],
                                                       unit=sample[2])
            sample_model.save()
        return super().form_valid(form)
