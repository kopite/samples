# -*- coding: utf-8 -*-
"""
.. module:: samples.forms
   :synopsis: Forms used for the samples app.

.. moduleauthor:: Mark McKinlay <mark.mckinlay.io>
"""
from django import forms


class ImportSamplesForm(forms.Form):
    sample_data = forms.FileField()

    def clean(self):
        sample_data = self.cleaned_data.get('sample_data')
        if not sample_data.name.endswith('csv'):
            raise forms.ValidationError(
                f'File {sample_data.name}is not a csv'
            )
        cleaned_samples = []
        for sample in sample_data:
            sample_list = sample.decode("utf-8").split(',')
            if len(sample_list) != 3:
                raise forms.ValidationError(
                    f'Sample contains incorrect data {sample_list}'
                )
            cleaned_samples.append(sample_list)
        self.cleaned_data['sample_data'] = cleaned_samples

